from django.db import models
import json
# Create your models here.

class Acteur(models.Model):
    name = models.CharField(max_length=60,default="")
    civ = models.CharField(max_length=9,default="")
    alias = models.CharField(max_length=60,default="")
    uid = models.CharField(max_length=60,default="")
    birth_date = models.DateField(blank=True, null=True)
    death_date = models.DateField(blank=True, null=True)
    birth_city = models.CharField(max_length=60,default="")
    birth_country = models.CharField(max_length=60,default="")
    job = models.CharField(max_length=60,default="")
    mandat = models.JSONField(default=dict)
    
    def __str__(self):
        return self.name


class Organe(models.Model):
    uid = models.CharField(max_length=60,default="")
    codeType = models.CharField(max_length=10,default="")
    libelle = models.CharField(max_length=60,default="")
    libelleEdition = models.CharField(max_length=60,default="")
    libelleAbrege = models.CharField(max_length=60,default="")
    libelleAbrev = models.CharField(max_length=60,default="")
    viMoDe = models.JSONField(default=dict)
    organeParent = models.CharField(max_length=60,default="")
    chambre = models.CharField(max_length=60,default="")
    regime = models.CharField(max_length=80,default="")
    legislature = models.CharField(max_length=60,default='')
    secretariat = models.JSONField(default=dict)


    
    def __str__(self):
        return self.name