# serializers.py
from rest_framework import serializers

from .models import Acteur

class ActeurSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Acteur
        fields = ('name','birth_date','birth_city','job','mandat')



