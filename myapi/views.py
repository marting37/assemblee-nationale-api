from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
import os, json
from pprint import pprint
from rest_framework import viewsets
from .models import Acteur, Organe
from .serializers import ActeurSerializer
from datetime import datetime
import pymongo
# Create your views here.
def index(request):
    return HttpResponse("<h1>Hello and welcome to my first <u>Django App</u> project!</h1>")

class ActeurViewSet(viewsets.ModelViewSet):
    queryset = Acteur.objects.all().order_by('name')
    serializer_class = ActeurSerializer

# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# mydb = myclient["myapi"]
# mycol = mydb["myapi_acteur"]
# mycol.drop()




# Now get/create collection name (remember that you will see the database in your mongodb cluster only after you create a collection)

def get_all_actor():
    path_to_json = '../json/depute/acteur/'
    # this finds our json files
    json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
    

    # we need both the json and an index number so use enumerate()
    for index, js in enumerate(json_files):
        with open(os.path.join(path_to_json, js)) as json_file:
            json_text = json.load(json_file)
            new_acteur = Acteur()
            if type(json_text["acteur"]["etatCivil"]["ident"]["nom"]) is not dict or type(json_text["acteur"]["etatCivil"]["ident"]["prenom"]) is not dict:
                new_acteur.name = json_text["acteur"]["etatCivil"]["ident"]["nom"] + " " + json_text["acteur"]["etatCivil"]["ident"]["prenom"]
                new_acteur.civ= json_text["acteur"]["etatCivil"]["ident"]["civ"]
            if type(json_text["acteur"]["etatCivil"]["ident"]["alpha"]) is not dict:
                new_acteur.alias = json_text["acteur"]["etatCivil"]["ident"]["alpha"]
            if type(json_text["acteur"]["uid"]["#text"]) is not dict:
                new_acteur.uid = json_text["acteur"]["uid"]["#text"]
            birth_date_str = json_text["acteur"]["etatCivil"]["infoNaissance"]['dateNais']
            birth_date_obj = datetime.fromisoformat(birth_date_str)
            new_acteur.birth_date = birth_date_obj
            if type(json_text["acteur"]["etatCivil"]["infoNaissance"]["villeNais"]) is not dict:
                new_acteur.birth_city = json_text["acteur"]["etatCivil"]["infoNaissance"]["villeNais"]
                new_acteur.birth_country = json_text["acteur"]["etatCivil"]["infoNaissance"]["paysNais"]
            if type(json_text["acteur"]["profession"]["libelleCourant"]) is not dict:
                new_acteur.job = json_text["acteur"]["profession"]["libelleCourant"]
            new_acteur.mandat = json_text["acteur"]["mandats"]["mandat"]
            new_acteur.save()
# get_all_actor()

def get_all_organe():
    path_to_json = '../json/depute/organe/'
    # this finds our json files
    json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
    

    # we need both the json and an index number so use enumerate()
    for index, js in enumerate(json_files):
        with open(os.path.join(path_to_json, js)) as json_file:
            json_text = json.load(json_file)
            new_organe = Organe()
            if 'chambre' in json_text['organe']:
                new_organe.chambre = json_text['organe']['chambre']
            new_organe.codeType = json_text['organe']['codeType']
            if 'legislature' in json_text['organe']:
                new_organe.legislature = json_text['organe']['legislature']
            new_organe.libelle = json_text['organe']['libelle']
            new_organe.libelleAbrege = json_text['organe']['libelleAbrege']
            new_organe.libelleAbrev = json_text['organe']['libelleAbrev']
            new_organe.libelleEdition = json_text['organe']['libelleEdition']
            new_organe.organeParent = json_text['organe']['organeParent']
            if 'regime' in json_text['organe']:
                new_organe.regime = json_text['organe']['regime']
            if 'secretariat' in json_text['organe']:
                new_organe.secretariat = json_text['organe']['secretariat']
            new_organe.uid = json_text['organe']['uid']
            new_organe.viMoDe = json_text['organe']['viMoDe']
            new_organe.save()
                        
# get_all_organe()
